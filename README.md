# To Install

## Via scoped registries
Follow https://docs.upm-packages.dev/en/usage to set up your scoped registry,
then add `com.my-company.my-custom-package` to your dependencies:

```
"dependencies": {
    "com.my-company.my-custom-package": "latest"
    ...
}
```

## Via gitlab url
Add the following to your dependencies:
```
"dependencies": {
    "com.my-company.my-custom-package": "https://gitlab.com/my-company/my-custom-package.git",
    ...
}
```
