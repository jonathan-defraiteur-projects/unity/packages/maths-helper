﻿using UnityEngine;

namespace JonathanDefraiteur.MathsHelper.Runtime
{
    public static class Vector3Extension
    {
        public static Vector3 ClosestPoint(this Vector3 _point, Vector3 _a, Vector3 _b)
        {
            return _a + Vector3.Project(_point - _a, _b - _a);
        }
        
        public static Vector3 ClosestPointClamp(this Vector3 _point, Vector3 _a, Vector3 _b)
        {
            Vector3 projection = _point.ClosestPoint(_a, _b);

            float segment = (_a - _b).sqrMagnitude;
            float toA = (projection - _a).sqrMagnitude;
            float toB = (projection - _b).sqrMagnitude;
            if (segment < toA || segment < toB)
                return toA > toB ? _b : _a;
            return projection;
        }

        public static float ProgressionBetween(this Vector3 _point, Vector3 _a, Vector3 _b)
        {
            Vector3 projection = _point.ClosestPoint(_a, _b);

            float segment = (_a - _b).sqrMagnitude;
            float toA = (projection - _a).sqrMagnitude;
            float toB = (projection - _b).sqrMagnitude;

            float uProgression = Mathf.Sqrt(toA / segment);
            // Out of segment
            if (segment < toA || segment < toB)
                return toA > toB ? uProgression : -uProgression;
            // Under segment
            return uProgression;
        }

        public static float ProgressionBetweenClamp(this Vector3 _point, Vector3 _a, Vector3 _b)
        {
            return Mathf.Clamp01(_point.ProgressionBetween(_a, _b));
        }

        public static float ProgressOnSpline(this Vector3 _point, Vector3[] _spline)
        {
            if (_spline.Length < 2)
                return 0;

            float viewedSegmentsLength = 0;
            float closerDistance = float.PositiveInfinity;
            float closerLength = 0;
            
            for (int i = 0; i < _spline.Length - 1; i++) {
                Vector3 a = _spline[i];
                Vector3 b = _spline[i + 1];
                Vector3 projection = _point.ClosestPointClamp(a, b);

                float segment = (a - b).sqrMagnitude;
                float toA = (projection - a).sqrMagnitude;

                float distance = Vector3.Distance(_point, projection);
                if (closerDistance >= distance) {
                    closerDistance = distance;
                    closerLength = viewedSegmentsLength + Mathf.Sqrt(toA);
                }
                
                // Anyway
                viewedSegmentsLength += Mathf.Sqrt(segment);
            }

            return closerLength / viewedSegmentsLength;
        }

        public static Vector3 ClosestPointOnSpline(this Vector3 _point, Vector3[] _spline)
        {
            if (_spline.Length < 2)
                return _spline.Length == 1 ? _spline[0] : _point;
            
            float closerDistance = float.PositiveInfinity;
            Vector3 closerProjection = _spline[0];

            for (int i = 0; i < _spline.Length - 1; i++) {
                Vector3 projection = _point.ClosestPointClamp(_spline[i], _spline[i + 1]);

                float distance = Vector3.Distance(_point, projection);
                if (closerDistance >= distance) {
                    closerDistance = distance;
                    closerProjection = projection;
                }
            }

            return closerProjection;
        }
    }
}
